"""
pre-calculated program designed to generate c-files around virtualbox library

after much debugging, this library should now override all exceptions
that could not follow standard generation procedure and will generate
a single c-source file and c-header file for the entire virtualbox' library
"""
from __future__ import unicode_literals
import io
import json
import pycgen

#** Variables **#
# this dictionary decides exactly what function will be overwritten,
# the list is every argument name that should be an output array rather than an
# input array and '*' means that all arguments are output arrays
with open('override.json', 'r') as f:
    array_overide = json.load(f, encoding='utf-8')

#** Functions **#


def generate(xpcom_header, basedir='.', test=False):
    """
    generate c-src code which acts as a functional wrapper around vboxs macro
    based api located at "xpcom_header"
    """
    # read xpcom source code
    with open(xpcom_header, 'rb') as f:
        xpcom_data = f.read()
    # open files for c-source code and c-headers
    c_file = io.open(basedir+'/c/src/vbox.c', 'w', encoding='utf-8')
    h_file = io.open(basedir+'/c/head/vbox.h', 'w', encoding='utf-8')
    # write inclusion of primary virtualbox header object
    c_file.write('#include "VBoxCAPIGlue.h"\n\n')
    h_file.write('#include "VBoxCAPIGlue.h"\n\n')
    # attempt to pull all structs from source code
    struct_names = pycgen.parse.find_xpcom_structs(xpcom_data)
    struct_names = [
        name
        for name in struct_names
        if not name.startswith('ns')
    ]
    # iterate all struct objects and generate function wrappers for them
    # along with associated header definition
    for struct_name in struct_names:
        # write prefix to declare what struct we are on
        lines = [
            '/**%s**/\n' % ('*'*len(struct_name)),
            '/* %s */\n' % struct_name,
            '/**%s**/\n' % ('*'*len(struct_name))
        ]
        c_file.writelines(lines)
        h_file.writelines(lines)
        # parse function object from function definitions in source code
        functions = pycgen.parse.parse_xpcom_struct(xpcom_data, struct_name)
        functions.append(pycgen.c_gen.get_release_func(struct_name))
        # iterate functions related to the current struct and generate code
        for func in functions:
            # ensure ns objects are not used in function arguments
            if any(arg for arg in func.args if arg.type.startswith('ns')):
                continue
            # ignore internal only functions
            if 'internal' in func.name.lower():
                continue
            if '#define %s_%s(' % (struct_name, func.name) not in xpcom_data:
                continue
            # perma-block internal weird function
            if 'QueryBitmapInfo' in func.name:
                continue
            # generate c-code with function-generator
            cfunc = pycgen.c_gen.FunctionGenerator(
                function=func,
                old_prefix=struct_name,
                # new function name cannot be same
                struct_name=struct_name[1:],
                array_overide=array_overide,  # as macros, so remove 'I' from name
            )
            if test:
                c_src = cfunc.generate_with_tests(cwd=basedir+'/c/')
            else:
                c_src = cfunc.generate()
            c_file.write(c_src + '\n')
            c_file.flush()
            # generate header based on newly generated c source code
            c_src_func = pycgen.parse.parse_source_functions(c_src)[0]
            c_head = c_src_func.to_header()
            h_file.write(c_head + ';\n')
            h_file.flush()
        # apppend event conversion functions to replace QueryInterface
        # since golang cant handle that level of dynamic type input
        # (at least as far as I know, im not a C wizard)
        if struct_name.endswith('Event') and struct_name != 'IEvent':
            code = pycgen.c_gen.gen_event_conversion(struct_name)
            header = code.split('{', 1)[0].strip()
            c_file.write(code)
            h_file.write(header+';\n')
            c_file.flush()
            h_file.flush()

    # close files on completion
    c_file.flush()
    c_file.close()
    h_file.flush()
    h_file.close()
