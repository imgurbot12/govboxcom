"""
pre-calculated program designed to wrap c-src-files with golang and build a library

this will take the header output created from generate_c.py and generate
a complete library in the "go/" folder
"""
from __future__ import unicode_literals
import io
import json
import pycgen

#todo: test that generated library actually works before you go refactoring
# or completing anything else. need to make sure all this hard-work will actually
# pay off. sheesh...

#** Variables **#
# array override dictionary -> better described in generate_c.py
with open('override.json', 'r') as f:
    array_overide = json.load(f, encoding='utf-8')

_code = """package {package}
/*
#cgo CFLAGS: -I./c/head -I./c/bindings/c/include -I./c/bindings/c/glue
#cgo LDFLAGS: -L./c/ -lvbox -ldl -lpthread

#include <stdlib.h>
#include "vbox.h"
#include "shared.h"
*/
import "C"
import (
    "fmt"
    "unsafe"
    "reflect"
)

/* Variables */
{vars}

/* Functions */
{funcs}

/* Methods */
{methods}
"""

#** Functions **#


def _find_objects(xpcom_header, basedir='.'):
    """
    collect dictionary containing all valid structs
    and all generated functions connected to those structs
    """
    # read source file to collect struct-names
    with open(xpcom_header, 'rb') as f:
        xpcom_data = f.read()
    # read header file to find all generated functions
    with io.open(basedir+'/c/head/vbox.h', 'r', encoding='utf-8') as f:
        header_data = str(f.read())
    # collect struct-names
    struct_names = pycgen.parse.find_xpcom_structs(xpcom_data)
    # collect all functions
    all_functions = pycgen.parse.parse_header_functions(header_data)
    # correlate function-names to struct-names
    struct_out = {name: [] for name in struct_names}
    for func in all_functions:
        # re-generate correlated struct-name based on new func name
        struct_name = func.name.split('_')[0]
        if not struct_name.startswith('s'):
            struct_name = str('I'+struct_name)
        else:
            struct_name = str('n'+struct_name)
        # append function to correlated struct
        struct_out[struct_name].append(func)
    # collect enums from c-src code
    enums = pycgen.parse.parse_enums(xpcom_data)
    # return completed dictionary
    return struct_out, enums


def _build_new_struct(struct_name):
    """
    build new golang struct wrapper for original c-interface object
    """
    return """type {0} struct {{ c *C.{0} }}""".format(struct_name)


def _build_new_func(struct_name):
    """
    build the associated new function to initialize an object w/ a base c-object
    """
    return """func new{0}(c *C.{0}) *{0} {{
    if c != nil {{
        return &{0}{{c: c}}
    }}
    return nil
}}
""".format(struct_name)


def generate(xpcom_header, basedir='.', pkg='xpcom'):
    """
    generate golang wrapper for c-source code generated with generate_c.py
    """
    # find list of structs and correlated functions
    structs, enums = _find_objects(xpcom_header, basedir=basedir)
    # open go-file to write newly generated code to
    g_file = io.open('{0}/{0}.go'.format(pkg), 'w', encoding='utf-8')
    # create variables for output
    vars = []
    funcs = []
    methods = []
    # spawn function code for every primary interface object
    for struct_name, functions in structs.items():
        # generate assocated struct object
        vars.append(
            _build_new_struct(struct_name)
        )
        funcs.append(
            _build_new_func(struct_name)
        )
        # attempt generate golang-function code
        for func in functions:
            gfunc = pycgen.go_gen.FunctionGenerator(
                function=func,
                old_prefix=struct_name[1:],
                struct_name=struct_name,
                array_overide=array_overide,
            )
            gsrc = gfunc.generate()
            methods.append(gsrc)
    # spawn enums for every primary enum object
    for enum in enums:
        gsrc = enum.to_go()
        vars.append(gsrc)
    # build final code output with generated components
    code = _code.format(
        package=pkg,
        vars='\n'.join(vars),
        funcs='\n'.join(funcs),
        methods='\n'.join(methods)
    )
    g_file.write(code)
    g_file.flush()
    g_file.close()
