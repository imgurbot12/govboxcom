#include "VBoxCAPIGlue.h"

/* Error Checking */
HRESULT cFAILED(HRESULT res);

/* Memory Functions */
void freeUtf8(char* string);
HRESULT freeArrayOut(void* array);
HRESULT freeCOMString(BSTR instr);
HRESULT convUtf16ToUtf8(BSTR instr, char** outstr);
HRESULT convUtf8ToUtf16(char* instr, BSTR* outstr);

/* Array Functions */
SAFEARRAY* createBSTRVectorArray(BSTR** inArray);
SAFEARRAY* createIMachineVectorArray(IMachine** inArray, ULONG mediaCount);
SAFEARRAY* createEmptyBSTRVectorArray();
void releaseVectorArray(SAFEARRAY* array);

/* Init Functions */
HRESULT cGlueInit();
HRESULT initClient(IVirtualBoxClient** client);
HRESULT initVirtualBox(IVirtualBoxClient* client, IVirtualBox** vbox);

/* De-Init Functions */
void cGlueDeInit();
void deinitClient();
HRESULT deinitVirtualBox(IVirtualBox* vbox);
