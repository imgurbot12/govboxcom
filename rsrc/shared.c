#include "VBoxCAPIGlue.h"

/* Error Checking */
HRESULT cFAILED(HRESULT res) {
    return FAILED(res);
}

/* Memory Functions */
void freeUtf8(char* string) {
    g_pVBoxFuncs->pfnUtf8Free(string);
}

HRESULT freeArrayOut(void* array) {
    return g_pVBoxFuncs->pfnArrayOutFree(array);
}

void freeCOMString(BSTR instr) {
    g_pVBoxFuncs->pfnComUnallocString(instr);
}

HRESULT convUtf16ToUtf8(BSTR instr, char** outstr) {
    return g_pVBoxFuncs->pfnUtf16ToUtf8(instr, outstr);
}

HRESULT convUtf8ToUtf16(char* instr, BSTR* outstr) {
    return g_pVBoxFuncs->pfnUtf8ToUtf16(instr, outstr);
}

/* Init Functions */
HRESULT cGlueInit() {
    return VBoxCGlueInit();
}

HRESULT initClient(IVirtualBoxClient** client) {
    return g_pVBoxFuncs->pfnClientInitialize(NULL, client);
}

HRESULT initVirtualBox(IVirtualBoxClient* client, IVirtualBox** vbox) {
    return IVirtualBoxClient_GetVirtualBox(client, vbox);
}

/* De-Init Functions */
void cGlueDeInit() {
    return VBoxCGlueTerm();
}

void deinitClient() {
    g_pVBoxFuncs->pfnClientUninitialize();
}

HRESULT deinitVirtualBox(IVirtualBox* vbox) {
    return IVirtualBox_Release(vbox);
}
