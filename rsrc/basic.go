package pkgnamehere

/*
#cgo CFLAGS: -I../c/head -I../c/bindings/c/include -I../c/bindings/c/glue
#cgo LDFLAGS: -L../c/ -lvbox -ldl -lpthread

#include <stdlib.h>
#include "vbox.h"
#include "shared.h"
*/
import "C"
import (
	"fmt"
	"log"
	"os"
)

/* Variables */
var glueInit bool
var client *C.IVirtualBoxClient

// vboxlog : library logging instance to report un-expected but not fatal errors
//  or other important details happening behind the scenes in this library
var vboxlog = log.New(os.Stdout, "vbox - ", log.Ldate|log.Ltime)

/* Functions */

//Init : initialize the capi and return a wrapped IVirtualBoxClient on success
func Init() (*IVirtualBoxClient, error) {
	// check if glue already init
	if !glueInit {
		// init glue
		result := C.cGlueInit()
		if C.cFAILED(result) != 0 {
			cmessage := C.GoString(&C.g_szVBoxErrMsg[0])
			return nil, fmt.Errorf("VBoxCGlueInit Failed: %s", cmessage)
		}
		glueInit = true
	}
	// init client if not already
	if client == nil {
		if result := C.initClient(&client); C.cFAILED(result) != 0 || client == nil {
			return nil, fmt.Errorf("pfnClientInitalize Failed: %x", result)
		}
	}
	// return wrapped client instance
	return newIVirtualBoxClient(client), nil
}

//DeInit : de-initialize the virtualbox-api and exit cleanly
func DeInit() {
	// release client
	if client != nil {
		C.deinitClient()
		client = nil
	}
	// complete de-init
	if glueInit {
		C.cGlueDeInit()
		glueInit = false
	}
}

//cBoolToBool : convert c-bool to golang-bool
func cBoolToBool(b C.PRBool) bool {
	return b == 1
}

//boolToCBool : convert golang-bool to c-bool
func boolToCBool(b bool) C.PRBool {
	var cb C.PRBool
	if b {
		cb = 1
	}
	return cb
}

//bSTRToString : convert BSTR instance to string
func bSTRToString(bstr C.BSTR) string {
	var char *C.char
	result := C.convUtf16ToUtf8(bstr, &char)
	if result != 0 {
		vboxlog.Printf("WARNING - BSTR UTF-16 -> UTF-8 failed: %x", result)
		return ""
	}
	var gostr = C.GoString(char)
	C.freeUtf8(char)
	return gostr
}

//strToBSTR : convert string to BSTR instance
func strToBSTR(str string) C.BSTR {
	var char = C.CString(str)
	var bstr C.BSTR
	result := C.convUtf8ToUtf16(char, &bstr)
	if result != 0 {
		vboxlog.Printf("WARNING - CHAR UTF-8 -> UTF-16 failed: %x", result)
		return nil
	}
	C.freeUtf8(char)
	return bstr
}
