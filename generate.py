import os
import re
import sys
import cli
import time
import shutil
import zipfile
import requests
import subprocess
from bs4 import BeautifulSoup

import generate_c
import generate_go

#** Variables **#
_downloads = 'https://download.virtualbox.org/virtualbox/'
_symver = re.compile(r'^(?:\d{1,3}\.){1,2}\d{1,3}(?:\.\w+)?$')

# cache for versions
_versions = []

#** Functions **#


def _list_versions():
    """
    get list of versions available via downloads website
    """
    global _versions
    if not _versions:
        # complete versions only if never completed lookup
        r = requests.get(_downloads)
        if r.status_code != 200:
            raise Exception('unable to reach server[%d]: %s' % (
                r.status_code, r.text
            ))
        # retrieve versions
        soup = BeautifulSoup(r.text, 'html.parser')
        for anchor in soup.find_all('a'):
            # pull anchor
            href = anchor['href']
            href = href[:-1] if href.endswith('/') else href
            # match symvers
            match = _symver.match(href)
            if match is not None:
                _versions.append(match.group())
        # sort versions
        _versions = sorted(_versions)
    return _versions

#** Classes **#


class VBoxAPIBuilder:
    """
    builds a complete wrapper around an existing virtualbox-api
    based on the given version number
    """

    def __init__(self, version):
        self.version = version
        # check if version exists
        all_versions = _list_versions()
        if version not in all_versions:
            raise Exception('version: %s not in available versions!\n - %s' % (
                version, '\n - '.join(all_versions)
            ))

    @staticmethod
    def _request(url, *args, **kwargs):
        """
        complete request and check for error
        """
        r = requests.get(url, *args, **kwargs)
        if r.status_code != 200:
            raise Exception('unable to reach server[%d]: %s' % (
                r.status_code, r.text
            ))
        return r

    def _download_file(self, url, fname=None, msg='downloading'):
        """
        download the file to the given location
        """
        # setup spinner
        spinner = ['|', '/', '-', '\\', '|']
        n, max = 0, len(spinner)-1
        # set filename
        if fname is None:
            fname = url.split('/')[-1]
        # complete download
        tick = 0
        with self._request(url, stream=True) as r:
            with open(fname, 'wb') as f:
                for chunk in r.iter_content(chunk_size=8192):
                    if chunk:
                        f.write(chunk)
                        f.flush()
                        # show spinner
                        if time.time()-tick >= 0.1:
                            n = (n+1) % max
                            sys.stdout.write('\r%s %s' % (msg, spinner[n]))
                            sys.stdout.flush()
                            tick = time.time()
        n = (n+1) % max
        sys.stdout.write('\r%s %s\n' % (msg, spinner[n]))
        sys.stdout.flush()
        tick = time.time()
        # return filename incase it was generated
        return fname

    def _find_sdk(self):
        """
        find sdk from listings in specific version
        """
        r = self._request(_downloads+self.version+'/')
        soup = BeautifulSoup(r.text, 'html.parser')
        for anchor in soup.find_all('a'):
            if 'virtualboxsdk' in anchor['href'].lower():
                return _downloads+self.version+'/'+anchor['href']
        # else raise Exception
        raise Exception('unable to find sdk!')

    def _download_sdk(self, build):
        """
        download sdk for associated virtualbox version
        """
        # find sdk and download it
        fpath = self._download_file(
            url=self._find_sdk(),
            fname=build+'/temp/sdk.zip',
            msg='downloading sdk',
        )
        # attempt to unzip path
        dir = os.path.dirname(fpath)
        with zipfile.ZipFile(fpath) as zip:
            zip.extractall(path=dir)
        return dir

    def _copy_sdk(self, sdkpath, newpath):
        """
        copy required sdk resources to separate location
        """
        # ensure paths both exist
        if not os.path.exists(sdkpath):
            raise Exception('sdkpath: %s does not exist!' % sdkpath)
        if not os.path.exists(newpath):
            os.makedirs(newpath)
        # determine location of required c/include/
        capi_header = sorted([
            sdkpath+'/c/include/'+f
            for f in os.listdir(sdkpath+'/c/include/')
            if f.endswith('.h')
        ])[-1]
        # make a include directory with all required resources
        header_name = os.path.basename(capi_header)
        os.makedirs(newpath+'/c/include/')
        shutil.copy(capi_header, newpath+'/c/include/'+header_name)
        shutil.copytree(sdkpath+'/c/glue/', newpath+'/c/glue/')
        shutil.copytree(sdkpath+'/mscom/lib/', newpath+'/mscom/lib/')
        shutil.copytree(sdkpath+'/xpcom/lib/', newpath+'/xpcom/lib/')
        shutil.copytree(sdkpath+'/mscom/include/', newpath+'/mscom/include')
        shutil.copytree(sdkpath+'/xpcom/include/', newpath+'/xpcom/include')
        # return name of header file as its the only variable
        return newpath+'/c/include/'+header_name

    @staticmethod
    def _copy_rsrc(build):
        """
        copy static and prepared resources into build
        """
        os.mkdir(build+'/c/src')
        os.mkdir(build+'/c/head')
        with open('rsrc/basic.go', 'r') as rf, open(build+'/basic.go', 'w') as wf:
            wf.write(rf.read().replace('pkgnamehere', build))
        shutil.copy('rsrc/shared.c', build+'/c/src/shared.c')
        shutil.copy('rsrc/shared.h', build+'/c/head/shared.h')
        shutil.copy('rsrc/Makefile', build+'/c/Makefile')

    def _generate_src(self, build, vbox_header, testc=False, testgo=False):
        """
        generate source code given arguments and complete tests if nessesary
        """
        print('generating c-src code' + ('w/ tests' if testc else ''))
        generate_c.generate(vbox_header, basedir=build, test=testc)
        print('generating go-src code')
        generate_go.generate(vbox_header, basedir=build, pkg=build)
        # run make on c-src code
        print('building c-src code w/ make')
        p = subprocess.Popen(
            args=['make'],
            cwd=build+'/c',
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
        )
        out = p.communicate()[0]
        if p.poll() != 0:
            raise Exception('build failed: %s' % out)
        # test go-build if requested
        if testgo:
            print('testing go-src code w/ "go build"')
            env = os.environ.copy()
            env['LD_LIBRARY_PATH'] = 'c/'
            p = subprocess.Popen(
                args=['go', 'build', '.'],
                cwd=build,
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT,
                env=env,
            )
            out = p.communicate()[0]
            if p.poll() != 0:
                raise Exception('build failed: %s' % out)

    def build(self, build='govbox', testc=False, testgo=False):
        """
        complete download and build of all required resources
        """
        print('generating api: %r for version: %s' % (build, self.version))
        # delete old build
        if os.path.exists(build):
            shutil.rmtree(build+'/')
        # make new build folder
        os.makedirs(build+'/temp/')
        # download sdk to temp dir in build
        dir = self._download_sdk(build)
        # copy important sdk resources into new directory
        print('copying sdk resources')
        vbox_header = self._copy_sdk(
            dir+'/sdk/bindings',
            os.path.dirname(dir)+'/c/bindings'
        )
        # remove temp dir
        shutil.rmtree(build+'/temp/')
        # copy required resources into required locations
        print('copying stored resources into build')
        self._copy_rsrc(build)
        # generate c-code and golang-code
        self._generate_src(build, vbox_header, testc=testc, testgo=testgo)
        print('finished!')

#** Start **#


def cli_handler(ctx):
    """
    cli handler for app
    """
    # list-versions overrides behavior
    if ctx.flag('list-versions'):
        versions = _list_versions()
        print('Versions:')
        for version in versions:
            print(' - %s' % version)
        return

    # ensure only one test flag set
    testc = ctx.flag('testc')
    testgo = ctx.flag('testg')
    testall = ctx.flag('test')
    if (testall and testc) or (testall and testgo) or (testgo and testc):
        ctx.on_usage_error('only one test flag may be present at once!')
    elif testall:
        (testgo, testc) = (True, True)

    # collect primary flags
    build = ctx.flag('pkg')
    version = ctx.flag('ver')
    if not version:
        ctx.on_usage_error('Flag: "version, v" is required!')

    # run build
    b = VBoxAPIBuilder(version)
    b.build(build, testc=testc, testgo=testgo)


if __name__ == '__main__':
    app = cli.App(
        name='VirtualBox Go API Generator',
        usage='generate go-bindings for any version of virtualbox!',
        version='2.1.0',
        flags=[
            cli.StringFlag(
                name='pkg, p',
                usage='specify package name for generated library',
                default='govbox',
            ),
            cli.StringFlag(
                name='ver, V',
                usage='specify version of virtualbox api to build',
            ),
            cli.BoolFlag(
                name='test, t',
                usage='run all tests on build for better debugging',
            ),
            cli.BoolFlag(
                name='testc, c',
                usage='run only tests on c-src code for debugging',
            ),
            cli.BoolFlag(
                name='testg, g',
                usage='run only tests on go-src code for debugging',
            ),
            cli.BoolFlag(
                name='list-versions, list',
                usage='list all versions of virtualbox available',
            )
        ],
        commands=[],
        action=cli_handler,
    ).run(sys.argv)
