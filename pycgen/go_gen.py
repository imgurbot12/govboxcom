from . import TAB, Function, _arg_is_output, _replace

#** Classes **#


class FunctionGenerator:
    """
    Generate a Golang Function from a C-Function
    """

    def __init__(self, function, old_prefix, struct_name, array_overide={}):
        """
        :param function:      parsed function object used to help build golang code
        :param old_prefix:    prefix from original function object in c-code that will be replaced
        :param struct_name:   new name declared
        :param array_overide: manual declaration of function-names that set all of the argument array types as output arrays
        """
        assert isinstance(function, Function)
        assert isinstance(old_prefix, str) and isinstance(struct_name, str)
        assert isinstance(array_overide, dict)
        # save pointer to function class
        self.f = function
        # save copy of old function name
        self.old_name = 'I' + self.f.name
        # save copy of array-override
        self.array_overide = array_overide
        # get copy of all arguments passed into function for later modification
        self.pass_args = function.args.pass_args()
        # get copy of all input function args for later modification
        self.args = function.args[:]
        # remove old c-name prefix from function name for sake of tying it to a new golang object
        self.func_name = function.name.replace(old_prefix+'_', '', 1)
        # get name of old-prefix and use it as the struct-pointer name
        self.struct_obj = old_prefix.lower()
        # cleanup array used to modify or complete conversion
        # between initial c-object and golang object
        self.cleanup = [
            TAB + 'if C.cFAILED(result) != 0 {\n',
            TAB*2
            + 'return %%sfmt.Errorf("%s %s Error: %%%%x", result)\n' % (struct_name, self.func_name),
            TAB + '}\n',
        ]
        # keep track of interfaces used
        self.interfaces = set()
        # return array, contains all objects to return from the golang-function
        self.returns = [('nil', 'error')]
        # start of new golang code based on original function object
        self.code = "func (%s *%s) %s(%%s) (%%s) {\n" % (
            self.struct_obj, struct_name, self.func_name)
        # fix all reserved golang names and replace them from function arguments
        for arg in self.f.args:
            # override type
            if arg.name == 'type':
                arg.name = 'dtype'
                self.pass_args = _replace(self.pass_args, 'type', 'dtype')
                # update args to new copy
                self.args = self.f.args[:]
            # override names same as struct-pointer
            if arg.name == self.struct_obj:
                name = 'v'+arg.name
                self.pass_args = _replace(self.pass_args, arg.name, name)
                arg.name = name
                # update args to new copy
                self.args = self.f.args[:]

    def _arg_is_output(self, arg):
        """
        return if argument should be treated as an output array rather than input
        """
        return _arg_is_output(self, arg)

    def _add_return(self, to_return):
        """
        add a return object into the appropriate location
        """
        if len(self.returns) == 1:
            self.returns.insert(0, to_return)
        else:
            self.returns.insert(len(self.returns)-1, to_return)

    def _get_output_array_types(self, arg):
        """
        get new types and associated conversion
        function used to go between c-types and
        golang types in order to retrieve the
        array values and convert it to golang
        """
        if arg.type.startswith('PRUint') or arg.type.startswith('PRInt'):
            go_object = 'int' + \
                arg.type[5:] if arg.type.startswith(
                    'PRInt') else 'uint' + arg.type[6:]
            return {
                'c-array':   '*C.%s' % arg.type,
                'c-object':  'C.%s' % arg.type,
                'go-object': go_object,
                'c-to-go':   go_object + '(%s)'
            }
        elif arg.type == 'PRBool':
            return {
                'c-array':   '*C.PRBool',
                'c-object':  'C.PRBool',
                'go-object': 'bool',
                'c-to-go':   'cBoolToBool(%s)'
            }
        elif arg.type == 'BSTR':
            return {
                'c-array': '**C.BSTR',
                'c-object': 'C.BSTR',
                'go-object': 'string',
                'c-to-go': 'bSTRToString(%s)'
            }
        elif arg.type == 'char':
            return {
                'c-array':   '**C.char',
                'c-object':  '*C.char',
                'go-object': 'string',
                'c-to-go':   'C.GoString(%s)',
            }
        else:
            self.interfaces.add(arg.type)
            return {
                'c-array':   '*'*(arg.star_count - 1) + 'C.%s' % (arg.type),
                'c-object':  '*C.%s' % arg.type,
                'go-object': '*' + arg.type,
                'c-to-go':   'new%s(%%s)' % arg.type
            }

    def _gen_output_array(self, num, arg):
        """
        generate next portion of the code assuming the given argument
        is an array meant to collect outputed variables from C
        """

        # remove size-argument and array argument from
        # golang input arguments because it is an output
        size_arg = self.args[num-1]
        self.f.args.remove(size_arg)
        self.f.args.remove(arg)

        # build size conversion from golang-type to c-type for entering into function call
        if size_arg.type.startswith("PRUint"):
            count_conversion = 'uint' + size_arg.type[6:] + '(%s)'
        elif size_arg.type.startswith("PRInt"):
            count_conversion = 'int' + size_arg.type[5:] + '(%s)'
        else:
            count_conversion = 'int(%s)'

        # get new types used to convert c and golang types
        types = self._get_output_array_types(arg)

        # build empty array object with input pointer for retrieval
        self.code += TAB + 'var %s %s\n' % (arg.name, types['c-array'])
        self.code += TAB + \
            'var %s %sC.%s\n' % (size_arg.name, '*'
                                 * (size_arg.star_count-1), size_arg.type)

        self.pass_args = _replace(self.pass_args, arg.name, '&'+arg.name)
        self.pass_args = _replace(
            in_str=self.pass_args,
            og=size_arg.name,
            new='&'+size_arg.name
        )

        slice_name = arg.name+'Slice'
        self.cleanup.extend([
            TAB + '%s := *(*[]%s)(unsafe.Pointer(&reflect.SliceHeader{\n' % (
                slice_name, types['c-object']),
            TAB*2 + 'Data: uintptr(unsafe.Pointer(%s)),\n' % arg.name,
            TAB*2 + 'Len:  int(%s),\n' % size_arg.name,
            TAB*2 + 'Cap:  int(%s),\n' % size_arg.name,
            TAB + '}))\n',
            TAB + 'go%s := make([]%s, %s)\n' % (arg.name,
                                                types['go-object'], count_conversion % size_arg.name),
            TAB + 'for i := range %s {\n' % slice_name,
            TAB*2 + 'go%s[i] = %s\n' % (arg.name,
                                        types['c-to-go'] % (slice_name+'[i]')),
            TAB + '}\n',
            TAB + 'C.freeArrayOut(unsafe.Pointer(%s))\n' % arg.name,
        ])

        # set created-go array as return
        self._add_return(('go%s' % arg.name, '[]'+types['go-object']))

    def _get_input_array_types(self, arg):
        """
        generate conversion types and array types
        for required c-types and the original go-types
        that have been passed in
        """
        if arg.type.startswith('PRUint') or arg.type.startswith('PRInt'):
            out = {
                'go-type': '[]int' + arg.type[5:] if arg.type.startswith('PRInt') else '[]uint' + arg.type[6:],
                'go-to-c': 'C.%s(%%s)' % arg.type,
                'c-object': 'C.%s' % arg.type,
            }
        elif arg.type == 'PRBool':
            out = {
                'go-type': '[]bool',
                'go-to-c': 'boolToCBool(%s)',
                'c-object': 'C.PRBool'
            }
        elif arg.type == 'BSTR':
            out = {
                'go-type': '[]string',
                'go-to-c': 'strToBSTR(%s)',
                'c-object': 'C.BSTR'
            }
        elif arg.type == 'char':
            out = {
                'go-type':  '[]string',
                'go-to-c':  'C.CString(%s)',
                'c-object': '*C.char'
            }
        else:
            out = {
                'go-type': '[]*%s' % arg.type,
                'go-to-c': '%s.c',
                'c-object': '*C.%s' % arg.type
            }
        out['c-array-name'] = 'cArray%s' % arg.name.title()
        out['go-array-name'] = 'go%s' % arg.name.title()
        out['c-array'] = '*'*arg.star_count + 'C.%s' % arg.type
        return out

    def _gen_input_array(self, num, arg):
        """
        generate next portion of the code assuming the given argument
        is an array meant to send input arguments into the C function
        """

        # remove size-arg and replace pass_args with len determination
        size_arg = self.args[num-1]
        self.f.args.remove(size_arg)
        self.pass_args = _replace(
            in_str=self.pass_args,
            og=size_arg.name,
            new='C.%s(len(%s))' % (size_arg.type, arg.name)
        )

        # get new types to convert from golang array to c-array
        types = self._get_input_array_types(arg)

        # convert arg-type and star-count to allow proper conversion from golang-type to c-type
        arg.type = types['go-type']
        arg.star_count = 0

        # add code to generate c-array from golang object
        if 'BSTR' in types['c-array']:
            self.code += TAB + \
                '%s := C.malloc(C.size_t(len(%s)) * C.VT_BSTR)\n' % (
                    types['c-array-name'], arg.name)
        else:
            self.code += TAB + '%s := C.malloc(C.size_t(len(%s)) * C.size_t(unsafe.Sizeof(uintptr(0))))\n' % (
                types['c-array-name'], arg.name)
        self.code += TAB + '%s := (*[1<<30 - 1]%s)(%s)\n' % (
            types['go-array-name'], types['c-object'], types['c-array-name'])
        self.code += TAB + 'for idx, obj := range %s {\n' % arg.name
        self.code += TAB*2 + \
            '%s[idx] = %s\n' % (types['go-array-name'],
                                types['go-to-c'] % 'obj')
        self.code += TAB + '}\n'

        # modify pass-args to use new c-array
        first, _, last = self.pass_args.rpartition(arg.name)
        self.pass_args = first + \
            '(%s)(%s)' % (types['c-array'], types['c-array-name']) + last

        # add code for array cleanup after use
        self.cleanup.insert(
            0, TAB + 'C.free(unsafe.Pointer(%s))\n' % types['c-array-name'])

    def _handle_input_args(self, num, arg):
        """
        reformat and modify arguments that
        should be passed as inputs into the c-function
        and do not need to be returned at function exit
        """

        # update pThis to use struct c-object
        if arg.name == 'pThis':
            # remove arg from input vars
            self.f.args.remove(arg)
            # update pass-args to use struct c-object
            self.pass_args = _replace(
                in_str=self.pass_args,
                og='pThis',
                new='%s.c' % self.struct_obj
            )

        # handle input chars
        elif arg.prefix() == "char*":
            arg.type = "string"
            arg.star_count = 0
            # add code to convert string to char
            self.code += TAB + "var c{0} = C.CString({0})\n".format(arg.name)
            # update pass-args to use char rather than string name
            self.pass_args = _replace(self.pass_args, arg.name, 'c'+arg.name)
            # add cleanup function to remove char variable after completion
            self.cleanup.insert(
                0, TAB + 'C.free(unsafe.Pointer(c%s))\n' % arg.name)

        elif arg.prefix() == 'PRBool':
            arg.type = 'bool'
            self.code += TAB + 'var c%s C.PRBool\n' % arg.name
            self.code += TAB + 'if %s {\n' % arg.name
            self.code += TAB*2 + 'c%s = 1\n' % arg.name
            self.code += TAB + '}\n'
            self.pass_args = _replace(
                in_str=self.pass_args,
                og=arg.name,
                new='c'+arg.name
            )

        elif arg.prefix() == 'ULONG':
            arg.type = 'int'
            self.pass_args = _replace(
                in_str=self.pass_args,
                og=arg.name,
                new='C.ULONG(%s)' % arg.name,
            )

        # convert custom uint type
        elif arg.type.startswith("PRUint") and arg.star_count == 0:
            int_size = arg.type[6:]
            arg.type = 'uint' + int_size
            self.pass_args = _replace(
                in_str=self.pass_args,
                og=arg.name,
                new='C.PRUint%s(%s)' % (int_size, arg.name)
            )

        # convert custom int type
        elif arg.type.startswith("PRInt") and arg.star_count == 0:
            int_size = arg.type[5:]
            arg.type = 'int' + int_size
            self.pass_args = _replace(
                in_str=self.pass_args,
                og=arg.name,
                new='C.PRInt%s(%s)' % (int_size, arg.name)
            )

        # handle library c-types
        elif arg.type.startswith("I") and arg.star_count == 1:
            self.pass_args = _replace(self.pass_args, arg.name, arg.name+".c")
            self.interfaces.add(arg.type)

    def _handle_output_args(self, num, arg):
        """
        reformat and modify arguments that should
        be passed in the c-function for the purpose
        of retrieving variables before returning
        their golang equivalents
        """
        if arg.type.startswith('I') and arg.star_count == 2:
            self.f.args.remove(arg)
            # replace with comma and ')' to ensure vars with similar names
            # dont get replaced instead
            self.pass_args = _replace(
                in_str=self.pass_args,
                og=arg.name,
                new='&'+arg.name,
            )

            # add code to create new instance to pull value
            self.code += TAB + 'var %s *C.%s\n' % (arg.name, arg.type)
            self.returns.insert(
                0, ('new%s(%s)' % (arg.type, arg.name), '*'+arg.type))
            self.interfaces.add(arg.type)

        elif arg.prefix() == "PRBool*":
            self.f.args.remove(arg)

            self.code += TAB + 'var c%s C.PRBool\n' % arg.name
            self.pass_args = _replace(
                in_str=self.pass_args,
                og=arg.name,
                new='&c'+arg.name
            )
            self._add_return((arg.name, 'bool'))

            self.cleanup.extend([
                TAB + 'var %s bool\n' % arg.name,
                TAB + 'if (c%s == 1) {\n' % arg.name,
                TAB*2 + ' %s = true\n' % arg.name,
                TAB + '}\n',
            ])

        elif arg.prefix() == "ULONG*":
            self.f.args.remove(arg)

            self.code += TAB + 'var c%s C.ULONG\n' % arg.name
            self.pass_args = _replace(
                in_str=self.pass_args,
                og=arg.name,
                new='&c'+arg.name
            )
            self._add_return(('int(c%s)' % arg.name, 'int'))

        elif arg.type.startswith("PRUint") and arg.star_count == 1:
            self.f.args.remove(arg)

            int_size = arg.type[6:]
            arg.type = 'uint' + int_size
            self.code += TAB + 'var c%s C.PRUint%s\n' % (arg.name, int_size)
            self.pass_args = _replace(
                in_str=self.pass_args,
                og=arg.name,
                new='&c%s' % arg.name
            )
            self.returns.insert(
                0, ('%s(c%s)' % (arg.type, arg.name), arg.type))

        elif arg.type.startswith("PRInt") and arg.star_count == 1:
            self.f.args.remove(arg)

            int_size = arg.type[5:]
            arg.type = 'int' + int_size
            self.code += TAB + 'var c%s C.PRInt%s\n' % (arg.name, int_size)
            self.pass_args = _replace(
                in_str=self.pass_args,
                og=arg.name,
                new='&c%s' % arg.name
            )
            self.returns.insert(
                0, ('%s(c%s)' % (arg.type, arg.name), arg.type)
            )

        elif arg.prefix() == "char**":
            self.f.args.remove(arg)

            self.pass_args = _replace(self.pass_args, arg.name, '&c'+arg.name)
            self.code += TAB + 'var c%s *C.char\n' % arg.name
            self._add_return((arg.name, 'string'))
            self.cleanup.extend([
                TAB + '{0} := C.GoString(c{0})\n'.format(arg.name),
                TAB + 'C.free(unsafe.Pointer(c%s))\n' % arg.name,
            ])

    def _gen_failed_returns(self):
        """
        generate returned variables that
        would return if the system failed
        at any point meaning to return empty
        values
        """
        failed = []
        for r in self.returns[:-1]:
            if r[1] == 'string':
                failed.append('""')
            elif '*I' in r[1]:
                failed.append('nil')
            elif 'int' in r[1] and '[]' not in r[1]:
                failed.append('0')
            elif 'bool' == r[1]:
                failed.append('false')
            else:
                failed.append('nil')
        return ', '.join(failed) + ', ' if failed else ''

    def generate(self):
        """
        complete golang code generation of function
        based on passed arguments and equivilent golang types
        with the easiest conversion
        """
        is_array = False  # array detection bool
        for n, arg in enumerate(self.args, 0):

            # if argument endswith 'Size', next arg is an array
            # and needs to be generated and parsed as such
            if arg.name.endswith('Size') and n+1 != len(self.args):
                # if next arg startswith the same name w/o 'Size'
                if self.args[n+1].name.startswith(arg.name[:-4]):
                    is_array = True
                    continue

            # handle array argument based on whether it is
            # an input function or an output function
            if is_array:
                is_array = False
                if self._arg_is_output(arg):
                    self._gen_output_array(n, arg)
                else:
                    self._gen_input_array(n, arg)

            # handle input variables
            self._handle_input_args(n, arg)

            # handle ouput variables
            self._handle_output_args(n, arg)

        # pass arguments into primary function and check for error
        self.code += TAB + \
            'result := C.%s(%s)\n' % (self.f.name, self.pass_args)
        self.code += ''.join(self.cleanup)
        self.code += TAB + \
            'return %s\n' % ', '.join(r[0] for r in self.returns)

        # complete function code
        self.code += '}\n\n'

        # add failed execution args if there are any and fix final code formatting issues
        self.code = self.code % (
            self.f.args.to_golang(),
            ', '.join(r[1] for r in self.returns),
            self._gen_failed_returns()
        )
        self.code = self.code.replace('(error) {', 'error {')

        # return complete code object
        return self.code
