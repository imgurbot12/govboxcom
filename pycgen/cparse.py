from . import Enum, Argument, ArgumentList, Function

#** Functions **#


def parse_function_definition(definition_str):
    """
    parse string containing definition of c function from struct into an object
    """
    # parse function-return-type, function-name, and arguments
    prefix, arg_str = definition_str.rsplit('(', 1)
    return_arg, func_name = prefix.split(' ', 1)
    func_name = func_name.strip("(").strip(")").strip("*")
    args = (arg.strip().replace('* *', '**')
            for arg in arg_str[:-1].split(','))

    # parse arguments for star-count and append to argument-list
    arg_list = ArgumentList()
    for arg in args:
        star_count = len([c for c in arg if c == '*'])
        arg = arg.replace('*', '')
        arg_type, arg_name = arg.split(' ', 1)
        arg_list.append(Argument(
                arg_name=arg_name,
                arg_type=arg_type,
                star_count=star_count
        ))

    return Function(func_name=func_name, return_arg=return_arg, args=arg_list)


def parse_xpcom_struct(source_code, struct_name):
    """
    retrieve functions linked with the given struct name by parsing source code
    """
    # parse source code for lines containing the struct definition
    vtbl = False
    lines = []
    struct_line = 'struct %s_vtbl' % struct_name
    for line in source_code.split('\n'):
        if struct_line in line:
            vtbl = True
        if vtbl:
            lines.append(line)
        if vtbl and line == '};':
            break

    # parse functions from definition lines using semicolons as delimiter
    functions = []
    def_string = "\n".join(lines)
    for definition in (d.strip() for d in def_string.split(';')):
        if definition.startswith('nsresult '):
            functions.append(
                parse_function_definition(definition)
            )

    return functions


def find_xpcom_structs(source_code):
    """
    find all struct names in source-code
    """
    structs = set(
        line.strip().split(' ')[1].split('_vtbl')[0]
        for line in source_code.split('\n')
        if 'struct ' in line and 'vtbl' in line
    )
    return sorted([st for st in structs])


def parse_source_functions(source_code):
    """
    parse a c-src or header file for c-function definitions
    """
    return [
        parse_function_definition(line[:-2])
        for line in source_code.split('\n')
        if line.startswith('HRESULT') and line.endswith('{')
    ]


def parse_header_functions(source_code):
    """
    parse a c-header file for c-function definitions
    """
    return [
        parse_function_definition(line[:-1])
        for line in source_code.split('\n')
        if line.startswith('HRESULT') and line.endswith(';')
    ]


def parse_enums(source_code):
    """
    parse all enums in source-code
    """
    # get lines containing enum defintions
    (enums, enum, found) = ([], [], False)
    for line in source_code.split('\n'):
        # find start of valid enum
        if line.startswith('typedef enum ') and 'PR_' not in line:
            if 'PR' not in line.split()[2]:
                found = True
        # append existing lines
        if found:
            enum.append(line)
        # find end of valid enum
        if found and line.startswith('}'):
            enums.append(enum)
            (enum, found) = ([], False)
    # parse components from enum
    enum_objects = []
    for enum in enums:
        name = enum[-1].split()[1][:-1]
        consts = [
            (const.strip(), value.strip().replace(',', ''))
            for const, value in (
                line.split('=')
                for line in enum[2:-1]
            )
        ]
        # collect type
        etype = 'PRUint32'
        for line in source_code.split('\n'):
            if '#define %s_T' % name in line:
                etype = line.split()[2]
                break
        # spawn enum object
        enum_objects.append(
            Enum(name=name, type=etype, constants=consts)
        )
    return enum_objects
