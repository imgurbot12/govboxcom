
#** Variables **#
__all__ = [
    'TAB',
    'c_gen',
    'go_gen',
    'parse',
    'Argument',
    'ArgumentList',
    'Function'
]

# tab size constant
TAB = '  '

_common_output = [
    'Take', 'Receive', 'Find', 'Get', 'Read', 'Query', 'Describe',
]
_common_output_prefix = [
    'return', 'affected',
]

#** Functions **#


def _arg_is_output(self, arg):
    """
    return if argument should be treated as an output array rather than input
    """
    assert isinstance(arg, Argument)
    # do soley based on override if given
    override_args = self.array_overide.get(self.old_name, None)
    if override_args is not None:
        return arg.name in override_args or override_args == '*'
    # check if any args have certain prefixes in them
    for prefix in _common_output_prefix:
        if any(a for a in self.f.args if a.name.startswith(prefix)):
            return arg.name.startswith(prefix)
    # else check if function name uses common prefix to be an output function
    return any(pfx for pfx in _common_output if self.func_name.startswith(pfx))


def _replace(in_str, og, new):
    """
    replace all instances of original with new in context of passed arguments
    """
    assert isinstance(in_str, str)
    assert isinstance(og, str)
    assert isinstance(new, str)
    # do modications to pass-arguments
    og_str = in_str
    in_str = in_str.replace(og+')', new+')', 1)
    in_str = in_str.replace(og+',', new+',', 1)
    if in_str.endswith(og) and og_str == in_str:
        in_str = in_str[:-len(og)] + new
    return in_str


#** Classes **#

class Enum:
    """
    holds both name, type, and constants of enum object and generates code accordingly

    this allows easy creation enum types for both c and golang code
    """

    def __init__(self, name, type, constants):
        self.name = name
        self.type = type
        self.constants = constants

    def to_go(self):
        """
        convert enum data to golang src-code
        """
        # convert type to golang
        gtype = 'uint32'
        if self.type != 'PRUint32':
            raise Exception('un-handled enum type: %s' % self.type)
        # spawn enum as const list
        code = '/* %s Enum */\n' % self.name
        code += 'const (\n'
        for const, value in self.constants:
            code += TAB + '%s %s = %s\n' % (const, gtype, value)
        code += ')\n\n'
        # spawn enum to string function which uses switch case statement
        code += 'func %s_ToString(value %s) string {\n' % (self.name, gtype)
        code += TAB + 'switch value {\n'
        # filter constants for duplicate values
        consts = {}
        for const, value in self.constants:
            if value not in consts:
                consts[value] = const
        # build switch with filtered constatnts
        for const in consts.values():
            code += TAB + 'case %s:\n' % const
            code += TAB*2 + 'return "%s"\n' % ('_'.join(const.split('_')[1:]))
        code += TAB + 'default:\n'
        code += TAB*2 + 'return "ERR_INVALID_CONSTANT:" + string(value)\n'
        code += TAB + '}\n'
        code += '}\n'
        return code


class Argument:
    """
    holds both the argument name and type of argument and generates code accordingly

    this allows easy creation of arguments in both c-form and golang-form
    while also being able to access and understand both the type and other
    important components of each argument which are used to generate code
    """

    def __init__(self, arg_type, arg_name, star_count):
        self.type = arg_type
        self.name = arg_name.strip()
        self.star_count = star_count

    def prefix(self):
        """
        return string of full type of argument as it was in c

        this is often used to help parse through and determine
        what to do with particular arguments
        """
        return "%s%s" % (self.type, '*'*self.star_count)

    def to_c(self):
        """
        return argument string as it would be in a c-function definition
        """
        return "%s %s" % (self.prefix(), self.name)

    def to_golang(self):
        """
        list single argument for golang with name and type, along with pointer stars.
        for lists, pointer stars go after []
        """
        return "%s %s%s" % (self.name, '*'*self.star_count, self.type)

    def __str__(self):
        """
        by default display the argument as if it was in c
        """
        return self.to_c()


class ArgumentList(list):
    """
    holds a series of arguments and formats for both golang and c

    this allows formation of arguments for both function argument
    definitions and passing the same arguments into other functions
    """

    def to_c(self):
        """
        return list of arguments for function defintion in c form
        """
        return ', '.join(obj.to_c() for obj in self)

    def to_golang(self):
        """
        return list of arguments for function defintion in golang form

        this overly complicated method only includes types for last argument
        in the series of arguments for every change in type. this is the most
        common form to list arguments for a function definition
        """
        out = []
        for n, arg in enumerate(self[:-1], 0):
            arg_str = arg.to_golang()
            # only include type if next type is different
            # else remove the type to preserve space
            if arg.prefix() == self[n+1]:
                arg_str = arg_str.split(' ')[-1]
            out.append(arg_str)
        # append last argument including type if there are any arguments
        out += [self[-1].to_golang()] if len(self) > 0 else []
        return ', '.join(out)

    def pass_args(self):
        """
        list arguments as if they were to be passed into a function

        this method is the same for both c and golang as it is simply a list
        of argument names that does not include types
        """
        return ', '.join(arg.name for arg in self)

    def append(self, object):
        """
        only allow appends of argument-types
        """
        assert isinstance(object, Argument)
        super(ArgumentList, self).append(object)

    def __str__(self):
        """
        by default display arguments as if they were in c
        """
        return self.to_c()


class Function:
    """
    holds all important contextual information related to a function definition

    this method also allows easy creation of c-wrappers to the associated
    c-macro as well as generates the according golang-function as well
    """

    def __init__(self, return_arg, func_name, args):
        self.name = func_name
        self.args = args
        self.return_arg = return_arg

    def to_header(self):
        """
        display as if was included in c-header file
        """
        return '%s %s(%s)' % (self.return_arg, self.name, self.args.to_c())

    def __str__(self):
        """
        by default display as c-header defintion
        """
        return self.to_header()


# direct access to code generation
import cparse as parse  # noqa: E402
import go_gen  # noqa: E402
import c_gen  # noqa: E402
