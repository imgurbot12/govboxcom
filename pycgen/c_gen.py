from __future__ import unicode_literals
import os.path
import subprocess
from . import TAB, Function, Argument, ArgumentList, _arg_is_output

#** Functions **#


def _fix_argument_name(fargs, pass_args, old_name, new_name):
    """
    fix all arguments with given name and convert them to valid name format
    """
    for arg in fargs:
        if arg.name == old_name:
            # replace argument name w/ new name
            pass_args = pass_args.replace(arg.name, new_name, 1)
            arg.name = new_name
            # replace associated size argument
            pass_args = _fix_argument_name(
                fargs, pass_args, old_name+'Size', new_name+'Size')
    # return new pass_args since strings are immutable
    return pass_args


def get_release_func(struct_name):
    """
    generate release c-function object
    """
    return Function(
        return_arg='HRESULT',
        func_name='Release',
        args=ArgumentList([
            Argument(
                arg_type=struct_name,
                arg_name='pThis',
                star_count=1,
            )
        ])
    )


def gen_event_conversion(struct_name):
    """
    generate event-conversion for any sub-class of IEvent
    """
    (name, obj) = (struct_name[1:], struct_name)
    return '\n'.join([
        'HRESULT Event_To%s(IEvent* pThis, %s** ev) {' % (name, obj),
        TAB
        + 'return IEvent_QueryInterface(pThis, &IID_%s, (void **)ev);' % obj,
        '}\n'
    ])

#** Classes **#


class FunctionGenerator:

    def __init__(self, function, old_prefix, struct_name, array_overide={}):
        """
        :param function:       function object used as basis for new code generation
        :param old_prefix:     name of old struct prefix to be discarded on new defintion
        :param struct_name:    name of struct that will be used for new definition prefix
        :param array_override: manual declaration of function-names that set all of the argument array types as output arrays
        """
        assert isinstance(function, Function)
        assert isinstance(old_prefix, str) and isinstance(struct_name, str)
        assert isinstance(array_overide, dict)
        # save pointer to function class
        self.f = function
        # save copy of function name as sepearate variable
        self.func_name = self.f.name
        # save old function full-name for override determination
        self.old_name = '%s_%s' % (old_prefix, function.name)
        # save copy of array-override
        self.array_overide = array_overide
        # get copy of all arguments passed into function for later modification
        self.pass_args = function.args.pass_args()
        # get copy of all input function args for later modification
        self.args = function.args[:]
        # get name of old-prefix and use it as the struct-pointer name
        self.struct_obj = old_prefix
        # cleanup array used to generate final code before exiting function
        self.cleanup = []
        # copy of all btr arguments, both in and out
        self.bstr_in = []
        self.bstr_out = []
        # start code w/ function definition
        self.code = 'HRESULT %s_%s(%%s) {\n' % (struct_name, function.name)
        # fix all reserved c names and replace them from function arguments
        self.pass_args = _fix_argument_name(
            fargs=function.args,
            pass_args=self.pass_args,
            old_name='result',
            new_name='cresult',
        )

    def _char_to_BSTR(self, char):
        """
        recursive function used to handle char to BSTR code generation

        this method is recursive because if the next BSTR conversion fails
        it must clean up all previus BSTR objects before exiting.
        """
        # generate declaration of empty BSTR
        code = TAB + 'BSTR w%s;\n' % char + TAB

        # if this is the first BSTR, include type for result on conversion
        if not self.bstr_in:
            code += 'HRESULT '

        # generate conversion line from char to BSTR
        code += 'result = g_pVBoxFuncs->pfnUtf8ToUtf16({0}, &w{0});\n'.format(
            char
        )

        # add cleanup on failed conversion
        code += TAB + 'if (FAILED(result))'
        code += TAB + '{\n' if self.bstr_in else '\n'
        code += ''.join(
            TAB * 2 + 'g_pVBoxFuncs->pfnUtf16Free(%s);\n' % bstr
            for bstr in self.bstr_in
        )
        code += TAB * 2 + 'return result;\n'
        code += (TAB + '}\n\n') if self.bstr_in else '\n'

        # update bstr_in and return code
        self.bstr_in.append('w%s' % char)
        return code

    def _handle_input_args(self):
        """
        convert and create code to handle all incoming non-array arguments
        """
        for arg in self.f.args:
            if arg.prefix() == 'PRUnichar*':
                arg.type = 'char'

                # add BSTR conversion for char
                self.code += self._char_to_BSTR(arg.name)

                # modify macro arguments to use converted BSTR
                self.pass_args = self.pass_args.replace(
                    arg.name, self.bstr_in[-1], 1)

                # add cleanup at end to remove the BSTR
                self.cleanup.append(
                    TAB
                    + 'g_pVBoxFuncs->pfnUtf16Free(%s);\n' % self.bstr_in[-1]
                )

    def _handle_output_args(self):
        """
        convert and create code to handle all outbound non-array arguments
        """
        for arg in self.f.args:
            if arg.prefix() == 'PRUnichar**':
                arg.type = 'char'

                # add null BSTR before macro to allow transfer of variable
                self.code += TAB + 'BSTR w%s;\n' % arg.name

                # modify arg-name to BSTR equivalent for passing arguments
                self.pass_args = self.pass_args.replace(
                    arg.name, '&w' + arg.name, 1)

                # add list of BSTRs used for conversion
                self.bstr_out.append('w' + arg.name)

    def _arg_is_output(self, arg):
        """
        return if argument should be treated as an output array rather than input
        """
        return _arg_is_output(self, arg)

    def _gen_output_array(self, num, arg, array_name, size_arg):
        """
        generate handler for output arrays meaning to retrieve data and return
        """
        # add code to generate SAFEARRAY
        self.code += TAB + 'SAFEARRAY *%s = g_pVBoxFuncs->pfnSafeArrayOutParamAlloc();\n' % array_name
        # modify arguments to include SAFEARRAY handling and remove passing of size argument
        # because size argument is only used to retrieve array size and not to be passed to function
        self.pass_args = self.pass_args.replace(size_arg + ', ', '')
        # under most circumstances, the array object used is two stars more than objects stored in the array
        self.pass_args = self.pass_args.replace(arg.name, 'ComSafeArrayAsOutIfaceParam(%s, %s %s)' % (
            array_name, arg.type if arg.type != 'BSTR' else 'PRUnichar', '*'
            * (arg.star_count - 2),
        ))
        # add cleanup to remove array if any result fails
        self.cleanup.extend([
            TAB + 'if (!FAILED(result)) {\n',
            TAB * 2 + 'result = g_pVBoxFuncs->pfnSafeArrayCopyOutIfaceParamHelper((IUnknown ***)%s, %s, %s);\n' % (
                arg.name, size_arg, array_name,
            ),
            TAB + '}\n',
        ])

    def _gen_input_array(self, num, arg, array_name, size_arg):
        """
        generate handler for input arrays meaning to pass values to macro
        """
        # add code to build SAFEARRAY with VirtualBox's type size handling
        self.code += TAB + 'SAFEARRAY *{0} = g_pVBoxFuncs->pfnSafeArrayCreateVector({1}, 0, {2});\n'.format(
            array_name, 'VT_UNKNOWN' if arg.type != 'BSTR' else 'VT_BSTR', size_arg,
        )

        # add code to pass original array values to new SAFEARRAY
        self.code += TAB + 'g_pVBoxFuncs->pfnSafeArrayCopyInParamHelper({0}, {1}, sizeof({2}{3}) * {4});\n'.format(
            array_name, arg.name, arg.type, '*'
            * (arg.star_count - 1), size_arg,
        )
        # modify arg-name to SAFEARRAY pass code, and remove size argument from pass-args
        self.pass_args = self.pass_args.replace(size_arg + ', ', '')
        self.pass_args = self.pass_args.replace(
            arg.name, 'ComSafeArrayAsInParam(%s)' % array_name)

    def _handle_all_arrays(self):
        """
        generate handlers for both input and output arrays
        """
        # handle input and output arrays
        is_array = False
        for n, arg in enumerate(self.args, 0):
            # if argument endswith 'Size', next arg is an array
            # and needs to be generated and parsed as such
            if arg.name.endswith('Size') and n + 1 != len(self.args):
                # if next arg startswith the same name w/o 'Size'
                if self.args[n+1].name.startswith(arg.name[:-4]):
                    is_array = True
                    continue
            # handle array argument based on whether it is
            # an input function or an output function
            if is_array:
                is_array = False

                # replace PRUnichar*** with BSTR***
                arg.type = 'BSTR' if arg.type == 'PRUnichar' else arg.type

                # define array-name and array size argument string once
                array_name = 'safeArray' + arg.name.title()
                size_arg = arg.name + 'Size'

                # generate array according to input vs output
                if self._arg_is_output(arg):
                    self._gen_output_array(n, arg, array_name, size_arg)
                else:
                    self._gen_input_array(n, arg, array_name, size_arg)

                # add SAFEARRAY removal / cleanup regaurdless of SAFEARRAY usage type
                self.cleanup.append(
                    TAB + 'g_pVBoxFuncs->pfnSafeArrayDestroy(%s);\n' % array_name)

    def _free_bstrs_on_error(self):
        """
        generate code to free all output BSTRs in case of failure
        """
        # free all BSTRs if result failed
        self.cleanup.append(TAB + 'if (FAILED(result)) {\n')
        self.cleanup.extend([
            TAB * 2 + 'g_pVBoxFuncs->pfnUtf16Free(%s);\n' % bstr
            for bstr in self.bstr_out
        ])
        self.cleanup.extend([
            TAB * 2 + 'return result;\n',
            TAB + '}\n\n'
        ])

    def _do_bstr_cleanup(self):
        """
        build cleanup for all bstrs near end of function definition
        """
        if not self.bstr_out:
            return
        # free all BSTRs if result failed
        self._free_bstrs_on_error()
        # add conversion from pre-defined BSTR to char output variable
        for bstr in self.bstr_out:
            self.cleanup.extend([
                TAB
                + 'result = g_pVBoxFuncs->pfnUtf16ToUtf8(%s, %s);\n' % (bstr, bstr[1:]),
                TAB + 'g_pVBoxFuncs->pfnUtf16Free(%s);\n' % bstr
            ])
            # check for conversion-error if not last BSTR conversion
            if bstr != self.bstr_out[-1]:
                self._free_bstrs_on_error()

    def generate(self):
        """
        generate c-function wrapper based on macro-definition
        """
        # handle all input arguments
        self._handle_input_args()
        # handle all input and output arrays
        self._handle_all_arrays()
        # handle all output arguments
        self._handle_output_args()
        # do bstr cleanup after building args
        self._do_bstr_cleanup()
        # pass function arguments not that types have been modified
        self.code = self.code % self.f.args
        # add macro function to code w/ difference based on cleanup
        if self.cleanup:
            # if result type was not already defined somewhere in function's code, add type specification
            self.code += TAB + \
                ('HRESULT ' if 'HRESULT' not in self.code[10:] else '')
            # add function call
            self.code += 'result = %s_%s(%s);\n' % (self.struct_obj,
                                                    self.f.name, self.pass_args)
            # add cleanup
            self.code += ''.join(self.cleanup)
            self.code += TAB + 'return result;\n'
        else:
            self.code += TAB + \
                'return %s_%s(%s);\n' % (self.struct_obj,
                                         self.f.name, self.pass_args)
        # close function and return generated code
        self.code += '}\n'
        return self.code

    def generate_with_tests(self, cwd='.'):
        """
        generate source code and test compile with make before continueing
        """
        cmd = "gcc -g -fPIC -Wall -I./bindings/c/include -I./bindings/xpcom/include -I./bindings/c/glue -o /dev/null -fdiagnostics-color -x c -c -"
        src = self.generate()
        code = b'#include "VBoxCAPIGlue.h"\n\n' + src
        print("testing: %s" % self.old_name)
        p = subprocess.Popen(
            args=cmd.split(),
            cwd=os.path.realpath(cwd),
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
        )
        out = p.communicate(code)
        poll = p.poll()
        if poll != 0:
            msg = "NON ZERO STATUS ERROR: %d\n" % poll
            msg += "CODE:\n%s" % str(code)
            msg += "ERROR:\n"
            msg += out[0].decode("utf-8")
            raise Exception(msg)
        return src
