#!/bin/bash

#** Functions **#

#get_versions : get largest of patch versions for each minor version to update
function get_versions {
  # collect complete list of versions from virtualbox's website
  VERSIONS=($(
    curl -s "https://download.virtualbox.org/virtualbox/" | \
    awk '/<a href="[0-9]+.[0-9]+.[0-9]+\// {print $2}' | \
    cut -d '>' -f2 | \
    cut -d '/' -f1 | \
    grep -vE '^4.'
  ))
  # get list of all versions "major.minor"
  MAJOR_MINOR=()
  for ver in "${VERSIONS[@]}"
  do
    minor=$(echo $ver | awk -F'.' '{print $1 "." $2}')
    MAJOR_MINOR=("${MAJOR_MINOR[@]}" $minor)
  done
  # do unique sort on them
  MAJOR_MINOR=($(
    printf '%s\n' "${MAJOR_MINOR[@]}" | awk -F'.' '{print $1 "." $2}' | sort -u
  ))
  # collect highest patch versions for each minor revision
  BEST_VERS=()
  for ver in "${MAJOR_MINOR[@]}"
  do
    printf "%s\n" "${VERSIONS[@]}" | grep $ver | tail -n 1
  done
}

#generate_binding : generate bindings for the particular version given
# and upload them to the correct branch on git
function generate_binding {
  # ensure argument is present
  if [[ -z $1 || -z $2 ]]; then
    echo "usage: generate_binding <git-folder> <version>"
    return 1
  fi
  ver=$2
  repo=$1
  # remove previous bindings folder if exists
  if [ -d "govbox" ]; then
    rm -r govbox/
  fi
  # attempt to generate bindings, testing for each one
  if ! python generate.py -V $ver -testg; then
    echo "failed to generate bindings!"
    return 1
  fi
  # prepare the git-repo by creating the required branches and remove old files
  cd $repo
  git branch "release/$ver"
  git checkout "release/$ver"
  git pull origin "release/$ver"
  rm -rf basic.go govbox.go c/
  # move newly generated files into git-repo and update git
  mv ../govbox/* .
  git add .
  git commit -m "automated updates to $ver build"
  git push origin "release/$ver"
  cd ..
}

#** Start **#

if [ ! -d govbox-repo/ ]; then
  echo "govbox git-repo not found, cloning repo"
  git clone https://gitlab.com/imgurbot12/govbox.git govbox-repo
fi

echo "collecting version canidates"
versions=($(get_versions))
for ver in "${versions[@]}"
do
  echo "generate: $ver"
  pwd
  generate_binding govbox-repo/ $ver
done
